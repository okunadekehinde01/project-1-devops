# Vpc
resource "aws_vpc" "Kehinde-vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "Kehinde-vpc"
  }
}


# Public subnet
resource "aws_subnet" "Public-Sub-1" {
  vpc_id     = aws_vpc.Kehinde-vpc.id
  cidr_block = "10.0.7.0/24"

  tags = {
    Name = "Public-Sub-1"
  }
}


# Public subnet
resource "aws_subnet" "Public-Sub-2" {
  vpc_id     = aws_vpc.Kehinde-vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "Public-Sub-2"
  }
}


# Private subnet
resource "aws_subnet" "Private-Sub-1" {
  vpc_id     = aws_vpc.Kehinde-vpc.id
  cidr_block = "10.0.3.0/24"

  tags = {
    Name = "Private-Sub-1"
  }
}


# Private subnet
resource "aws_subnet" "Private-Sub-2" {
  vpc_id     = aws_vpc.Kehinde-vpc.id
  cidr_block = "10.0.4.0/24"

  tags = {
    Name = "Private-Sub-2"
  }
}


# Public route table
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.Kehinde-vpc.id

  tags = {
    Name = "public-route-table"
  }
}


# Private route table
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.Kehinde-vpc.id

  tags = {
    Name = "private-route-table"
  }
}


# Public route table association
resource "aws_route_table_association" "public-route-1-association" {
  subnet_id      = aws_subnet.Public-Sub-1.id
  route_table_id = aws_route_table.public-route-table.id
}


# Public route table association
resource "aws_route_table_association" "public-route-2-association" {
  subnet_id      = aws_subnet.Public-Sub-2.id
  route_table_id = aws_route_table.public-route-table.id
}


# Private route table association
resource "aws_route_table_association" "private-route-1-association" {
  subnet_id      = aws_subnet.Private-Sub-1.id
  route_table_id = aws_route_table.private-route-table.id
}


# Private route table association
resource "aws_route_table_association" "private-route-2-association" {
  subnet_id      = aws_subnet.Private-Sub-2.id
  route_table_id = aws_route_table.private-route-table.id
}


# Internet gateway
resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.Kehinde-vpc.id

  tags = {
    Name = "IGW"
  }
}



resource "aws_route" "public-IGW-route" {
  route_table_id            = aws_route_table.public-route-table.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = aws_internet_gateway.IGW.id
}



